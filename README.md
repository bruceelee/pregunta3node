# # Instrucciones# #

1. Instalar mongodb, y dejar correr servicio mongod. (Por defecto trata de utilizar como ruta para database mongodb://@localhost:27017/testdatabase)
1. Utilizar comando : 'npm run create-db' desde el root del proyecto, esto intentará reconstruir las colleciones para la bd testdatabase en localhost.
1. En el caso que el paso dos no pueda ser posible, se puede construir la bd a partir de consultas REST, importar para eso desde POSTMAN el contenido de la carpeta 
/importToPostman

Se considera el CRUD básico (GET, POST, PUT, DELETE) para: 
```
 /api/teams (GET, POST)
 /api/teams/id (GET, PUT, DELETE)
 /api/players (GET, POST)
 /api/players/id (GET, PUT, DELETE)
```


4.Las preguntas se pueden consultar con los webservices de rutas:

 * Pregunta A) /api/teams/:idTeam/players
 * Pregunta B) /api/teams/players/active