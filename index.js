var express = require("express");
var mongoskin = require("mongoskin");
var bodyParser = require("body-parser");
var Q = require("q");

var app = express();

//Conexion local con mongodb
var db = mongoskin.db("mongodb://@localhost:27017/testdatabase", {safe:true});
var id = mongoskin.helper.toObjectID;


var allowMethods = function(req, res, next) {
	res.header('Access-Control-Allow-Methods', "GET, POST, PUT, DELETE, OPTIONS");
	next();
}

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(allowMethods);


//Para llenado de datos. 'coleccion' puede ser :[teams, players, etc,...]
app.param('coleccion', function(req, res, next, coleccion){
  req.collection = db.collection(coleccion);
  return next();
});

app.route('/api/:coleccion')
.get(function(req, res, next) {
  req.collection.find({},{
    limit:50, sort: [['_id', 1]]
  }).toArray(function(e, results){
    if (e) return next(e);
    res.send(results);
  });
}).post(function(req, res, next) {
  console.log(req.body);
  req.collection.insert(req.body, {}, function(e, results){
    if (e) return next(e);
    res.send(results);
  });
});

app.route('/api/:colecion/:id')
.get(function(req, res, next) {
  req.collection.findOne({
    _id: id(req.params.id)
  }, function(e, result){
    if (e) return next(e);
 	   res.send(result);
  });
})
.put(function(req, res, next) {
	req.collection.update({
	     _id: id(req.params.id)
	}, {$set:req.body}, {safe:true, multi:false},
	    function(e, result){
	      if (e) return next(e);
		  res.send((result === 1) ? {msg:'success'} : {msg:'error'});
	});
})
.delete(function(req, res, next) {
  req.collection.remove({
      _id: id(req.params.id)
    },
    function(e, result){
      if (e) return next(e);
      res.send((result === 1) ? {msg:'success'} : {msg:'error'});
	});
});


//Pregunta A) Para consulta especifica de jugadores de un equipo
app.param('idTeam', function(req, res, next, idTeam){
  req.players = db.collection('players');
  req.idTeam= idTeam;
  return next();
});

app.route('/api/teams/:idTeam/players')
  .get(function(req, res, next){
    var idTeam = req.idTeam; 

    req.players.find({team: idTeam},{
      limit:50, sort: [['_id',-1]]
    }).toArray(function(e, results){
      if (e) return next(e);
      res.send(results);
    });

});


//Pregunta B)
app.route('/api/teams/players/active')
  .get(function(req, res, next){

    req.teams = db.collection('teams');
    req.players= db.collection('players');

    function activeTeams(){
      var deferred = Q.defer();
      var status_activo= 1;

      req.teams.find({ status: status_activo},{ limit:50, sort: [['_id',1]]})
        .toArray(function(error, teams){
          if (error) {
              deferred.reject(new Error(error));
          }
          else {
              deferred.resolve(teams);
          }
      });

       return deferred.promise; 
    };

    function playersFromTeam(teamIds){
      var deferred = Q.defer();
      req.players.find({
          'team': { $in: teamIds }},{ limit:50, sort: [['_id',1]]})
        .toArray(function(error, players){
          if (error) {
              deferred.reject(new Error(error));
          }
          else {
              deferred.resolve(players);
          }
      });
       return deferred.promise; 
    };

  var id_teams= [];

  activeTeams()
    .then(function(teams){
      if(teams.length >0){
        teams.forEach(function(team){
          id_teams.push(team['_id']);
        });
      }
      else{
        res.send({msg:'No hay equipos activos'});
      }
      return playersFromTeam(id_teams);
    })
    .then(function(players){
      //Devuelvo los jugadores que encuentre
      res.send(players)
    })
    .catch(function(error){
        res.send({msg: 'error'});
    });
});

app.listen(8080, function(){
  console.log ('Servidor escuchando en puerto 8080');
});
